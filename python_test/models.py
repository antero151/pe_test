from django.core.validators import RegexValidator
from django.db import models


class CreateUpdate(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Client(CreateUpdate):
    name = models.CharField(max_length=50, unique=True)
    contact_name = models.CharField(max_length=50, default='', blank=True)
    street = models.CharField(max_length=100, default='', blank=True)
    suburb = models.CharField(max_length=50, default='', blank=True, db_index=True)
    postcode = models.CharField(max_length=10, default='', blank=True)
    state = models.CharField(max_length=10, default='', blank=True)
    email = models.EmailField(db_index=True)
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,16}$',
        message="Phone number must be entered in the format: '+9999999999'. 9 to 15 digits allowed."
    )
    phone = models.CharField(validators=[phone_regex], max_length=16)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name
