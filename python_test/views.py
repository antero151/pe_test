# Insert views here
from django.core.exceptions import FieldDoesNotExist
from django.db.models import Q
from django.views.generic import ListView, CreateView, UpdateView

from python_test.forms import ClientForm
from python_test.models import Client


class ClientListView(ListView):
    model = Client
    template_name = 'client/list.html'
    ordering = '-name'

    def get_ordering(self):
        ordering = self.request.GET.get('ordering')
        try:
            Client._meta.get_field(ordering[ordering.startswith('-') and 1 or 0:])
        except FieldDoesNotExist:
            pass
        except AttributeError:
            pass
        return ordering

    def get_queryset(self):
        search_query = self.request.GET.get('q')
        queryset = self.get_search_queryset(search_query)
        queryset = self.order_queryset(queryset)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ordering'] = self.request.GET.get('ordering')
        return context

    def order_queryset(self, queryset):
        if self.get_ordering():
            queryset = queryset.order_by(self.get_ordering())
        return queryset

    @staticmethod
    def get_search_queryset(search_query):
        if search_query:
            queryset = Client.objects.filter(
                Q(name__icontains=search_query) |
                Q(email__icontains=search_query) |
                Q(phone__icontains=search_query) |
                Q(suburb__icontains=search_query)
            )
        else:
            queryset = Client.objects.all()
        return queryset


class CreateClientView(CreateView):
    model = Client
    template_name = 'client/add_form.html'
    form_class = ClientForm
    success_url = '/clients/'


class UpdateClientView(CreateClientView, UpdateView):
    template_name = 'client/update_form.html'
